﻿using Mossad.Interfaces;

namespace Mossad.Models
{
    class TechnologyPopularity : ITechnologyPopularity
    {
        public int NumberOfVotes { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int NumberOfFans { get; set; }
        public string Logo { get; set; }
        public string Stacks { get; set; }
        public string Description { get; set; }
    }
}
