﻿using Mossad.Interfaces;

namespace Mossad.Models
{
    public class Technologies : ITechnoogies
    {
        public string TechnologyName { get; set; }
        public int YearsOfExperience { get; set; }
    }
}
