﻿using Mossad.Interfaces;

namespace Mossad.Models
{
    public class SpokenLanguages : ISpokenLanguages
    {
        public string Name { get; set; }
        public int Number { get; set; }
    }
}
