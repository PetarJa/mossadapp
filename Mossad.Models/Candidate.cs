﻿using Mossad.Interfaces;
using System;

namespace Mossad.Models
{
    public class Candidate : ICandidate
    {
        public ITechnoogies Technologies { get; set; }
        public ITechnologyPopularity TechnologyPopularity { get; set; }
        public ISpokenLanguages SpokenLanguages { get; set; }
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
        public int Age { get; set; }
        public int Size { get; set; }
        public string EyeColor { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CurrentCompany { get; set; }
        public string Picture { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Content { get; set; }
        public string FullResume { get; set; }
        public string Registered { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
