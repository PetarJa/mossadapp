﻿namespace Mossad.Interfaces
{
    public interface ITechnoogies
    {
        string TechnologyName { get; set; }
        int YearsOfExperience { get; set; }
    }
}
