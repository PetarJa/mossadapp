﻿using System;

namespace Mossad.Interfaces
{
    public interface ICandidate
    {
        ITechnoogies Technologies { get; set; }
        ITechnologyPopularity TechnologyPopularity { get; set; }
        ISpokenLanguages SpokenLanguages { get; set; }
        Guid Id { get; set; }
        bool IsActive { get; set; }
        int Age { get; set; }
        int Size { get; set; }
        string EyeColor { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string CurrentCompany { get; set; }
        string Picture { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        string Address { get; set; }
        string Content { get; set; }
        string FullResume { get; set; }
        string Registered { get; set; }
        string Latitude { get; set; }
        string Longitude { get; set; }
    }
}
