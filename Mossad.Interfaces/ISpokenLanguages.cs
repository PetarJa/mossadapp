﻿namespace Mossad.Interfaces
{
    public interface ISpokenLanguages
    {
        string Name { get; set; }
        int Number { get; set; }

    }
}
