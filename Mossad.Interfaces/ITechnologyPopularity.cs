﻿namespace Mossad.Interfaces
{
    public interface ITechnologyPopularity
    {
        int NumberOfVotes { get; set; }
        string Name { get; set; }
        string Url { get; set; }
        int NumberOfFans { get; set; }
        string Logo { get; set; }
        string Stacks { get; set; }
        string Description { get; set; }
    }
}
